from setuptools import setup

setup(
    name='gabor-scattering',
    version='0.0.4',
    author='Pavol Harar',
    author_email='pavol.harar@gmail.com',
    url='https://gitlab.com/paloha/gabor-scattering',
    description='Package for computation of Gabor Scattering transform.',
    long_description=open('readme.md').read(),
    packages=['gabor_scattering'],
    install_requires=['numpy>=1.11.0', 'librosa>=0.6.2', 'scikit-image>=0.14.1'],
    license='MIT',
)
