# Gabor Scattering

Python package for computing the Gabor Scattering according to https://arxiv.org/abs/1706.08818 and Mel Scattering according to https://arxiv.org/abs/1903.08950.

# Diagram of the algorithm

![gabor-scattering-0.0.4](/uploads/5bd34fdf13d58c7ba93ca666af179877/gabor-scattering-0.0.4.jpg)

# Transform visualization

![visualization](/uploads/c0083f536013a721bb3d7b939605ae82/visualization.png)
____
![grid](/uploads/47cd4ba5c9b629aae6a9a63048297048/grid.png)

# Installation

* ```python3 -m virtualenv .venv```
* ```source .venv/bin/activate```
* ```pip install git+https://gitlab.com/paloha/gabor-scattering```

or to install a specific version, let's say v0.0.1

* ```pip install git+https://gitlab.com/paloha/gabor-scattering@v0.0.1```

# Example
* ```jupyter notebook```
* open ```examples.ipynb```

# License
This project is licensed under the terms of the MIT license. See license.txt for details.


# Acknowledgement
This work was supported by International Mobility of Researchers (CZ.02.2.69/0.0/0.0/16027/0008371) and by project LO1401. The infrastructure of the SIX Center was used.

![opvvv](/uploads/53dc54bb8c5a8999a6866af162c70792/opvvv.jpg)
