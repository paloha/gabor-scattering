import numpy as np
from scipy.signal import stft
from librosa.feature import melspectrogram

"""
This is a new implementation implemented using scipy.signal.stft instead of
ltfatpy.gabor.dgt. Parameters changed to mirror the scipy implementation.

# L1 = gabor transform of the original signal (output is 2D)
# L2 = row-wise gabor transform of L1 (output is 3D)
# L2avg = L2 averaged in 0th axis (output is 2D)
# O1 = first row from each 2D matrix in L2 stacked together (output is 2D)
# O2 = each row of L2avg convolved with hann window, hence time averaged (output is same shape as L2avg)
# OutA = O1 resampled to desired size
# OutB = O2 resampled to desired size
# OutC = L1 resampled to desired size
"""

def gabor_transform(signal, nperseg=256, nfft=None, noverlap=None, axis=None, **kwargs):
    """
    Computes the stft of a signal and returns the real part of a half of the channels.
    Params as expected by https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.stft.html

    Parameters
    --------
    signal: 1D np.array
        Signal from which the stft will be computed.
        To compute Layer1 of scattering, signal.ndim must be 1 and axis = 0.
        To compute Layer2 of scattering, signal.ndim must be 2 and axis = 1.

    nperseg: int, optional, default 256
        Length of the window.

    nfft: int or None, optional
        Number of frequency channels.
        If None, the nfft is nperseg.

    noverlap: int, optional
        Number of points to overlap between segments.
        If None, noverlap = nperseg // 2.

    axis: int, optional, default None
        Axis along which the STFT is computed. E.g the last axis=-1.
        If None, which is the default, the axis will be set based on
        the dimension of the input for the purposes of the scattering:
            - Should equal 0 for computing layer1 of scattering.
            - Should equal 1 for computing layer2 of scattering.
    """

    # Infer the correct values
    axis = signal.ndim - 1 if axis is None else axis
    nfft = nperseg if nfft is None else nfft

    # Compute the transform
    x = abs(stft(signal, nperseg=nperseg, nfft=nfft, noverlap=noverlap,
                 axis=axis, **kwargs)[2])

    # Slice the exact half of the channels
    x = x[:nfft//2,:] if signal.ndim == 1 else  x[:,:nfft//2,:]

    return np.ascontiguousarray(x)


def get_o1(matrix3D):
    """
    Obtaining OUT1 - outputs 2D matrix which is a stack of first rows of layer2
    """
    return matrix3D[:,0,:]


def get_l2avg(matrix3D):
    """
    Obtaining averaged 2D representation of L2 by taking mean in 0th axis.
    """
    return np.mean(matrix3D, axis=0)


def time_averaging(signal, window_len=4, mode='same'):
    """
    Convolves signal with hann window of desired length.
    """
    win = np.hanning(window_len)
    return np.convolve(signal, win, mode=mode) / sum(win)


def get_o2(matrix, window_len=4, mode='same', axis=1):
    """
    Obtaining OUT2 - outputs 2D matrix which is a layer2avg
    convolved row-wise with hann window.

    Parameters
    --------

    matrix: 2D np.array
        Matrix that will be time averaged.

    window_len: int, optional, default=4
        Length of the hann window used for convolution.

    mode: str, optional, default='same'
        Mode of the convolution, one of {‘full’, ‘valid’, ‘same’}

    axis: int, optional, default=1
        Axis along which the convolution will be applied.
        Defaults to one, which stands for row-wise conv.
    """

    return np.apply_along_axis(time_averaging, # Func to apply
                               axis, # Along which axis to apply the func
                               matrix, # Data on which the func will be applied
                               window_len=window_len, # kwargs to func
                               mode=mode)


def resample_bilinear(matrix2D, newshape=None, order=1, mode='constant',
                      anti_aliasing=False, preserve_range=True, **resample_params):
    """
    Bilinear interpolation of 2D matrix using skimage package.
    For upsampling or downsampling of the final outputs.
    """
    if not newshape:
        return matrix2D

    from skimage.transform import resize
    return resize(matrix2D, newshape,
                  order=order, mode=mode,
                  preserve_range=preserve_range,
                  anti_aliasing=anti_aliasing,
                  **resample_params)


def gs(signal, l1_params={}, l2_params={}, mel_params=None,
       o2_params={}, shapes={}, resample_params={},
       return_layer1=False, return_layer2=False, return_l2avg=False,
       return_out1=False, return_out2=False, return_outA=True,
       return_outB=True, return_outC=True):

    """
    Computes Gabor scattering according to https://arxiv.org/pdf/1706.08818.pdf
    If mel_params are specified, computes Mel scattering.

    Parameters
    --------

    signal: numpy.array
        Signal to be processed with Gabor Scattering

    l1_params: dict, optional, default={}
        Dictionary of params for computation of the Layer1. For list of
        parameters see the signature of gabor_transform function.
        If {}, the default values of gabor_transform are used.

    l2_params: dict, optional, default={}
        Same parameters as in l1_params, just for computation of the Layer2.
        This should not be left to default.

    mel_params: dict, optional, default=None
        Dict of params for transforming the Layer1 into MEL spectrogram.
        If None, the transformation into MEL spectrogram is not done.
        If {}, the transformation into MEL spectrogram is done with defaults.
        For a list of parameters see:
        https://librosa.github.io/librosa/generated/librosa.filters.mel.html

    o2_params: dict, optional, default={}
        Dictionary of params for computation of the Output2. For list of
        parameters see the signature of get_o2 function.
        If {}, the default values of get_o2 are used.

    shapes: dict, optional, default={}
        Dictionary of desired shapes for 'outA', 'outB' and 'outC'.
        If set to {} as it is in default, the outA, outB and outC shapes
        will not be altered, thus will be the same as layer1, out1 and out2.
        Only those outputs specfied in the dictionary will be bilinearlly
        resampled to the desired size. All others are left as is.

        Structure:
        {'outA': (int, int),
         'outB': (int, int),
         'outC': (int, int)}

    resample_params: dict, optional, default={}
        Parameters to resample_bilinear(). If {}, defaults are used, see
        the function signature. For all other params, defaults of resize() re used.
        More here: http://scikit-image.org/docs/dev/api/skimage.transform.html#resize

    return_x: bool, optional
        If set to False, the particular output will not be included in the
        resulting dictionary. Instead, it will be set to None.


	Returns
    --------
    dict with keys ['layer1', 'layer2', 'l2avg', 'out1', 'out2', 'outA',
    'outB', 'outC']. All of the keys are always present, but the value
    can be None for one or more keys, if specified so in the parameters.
    """

    output = {'layer1': None, 'layer2': None, 'l2avg': None, 'out1': None,
              'out2': None, 'outA': None, 'outB': None, 'outC': None}

    # Obtaining LAYER1 - 2D matrix (original signal after Gabor transform)
    layer1 = gabor_transform(signal, **l1_params)

    # Converting to MEL for MEL scattering
    if mel_params is not None:
        layer1 = melspectrogram(S=layer1, **mel_params)

    # Resampling into desired size
    outA = resample_bilinear(layer1, shapes.get('outA'))

    output['layer1'] = layer1 if return_layer1 else None
    output['outA'] = outA if return_outA else None

    # Prevent unnecessary computation if only Gabor transform of Mel spectrogram is needed
    if not any([return_layer2, return_l2avg, return_out1, return_out2, return_outB, return_outC]):
        return output

    # Obtaining LAYER2 - 3D matrix (gabor transform of each row in LAYER1)
    layer2 = gabor_transform(layer1, **l2_params)
    output['layer2'] = layer2 if return_layer2 else None

    # Obtaining OUT1 - 2D matrix (first row of each 2D matrix in LAYER2)
    out1 = get_o1(layer2)
    output['out1'] = out1 if return_out1 else None

    # Obtaining L2avg - 2D matrix (L2 averaged in 0th axis)
    l2avg = get_l2avg(layer2)
    output['l2avg'] = l2avg if return_l2avg else None

    # Obtaining OUT2 - 2D matrix (Time averaged l2avg)
    out2 = get_o2(l2avg, **o2_params)
    output['out2'] = out2 if return_out2 else None

    # Resampling into desired sizes
    if return_outB:
        output['outB'] = resample_bilinear(out1, shapes.get('outA'))
    if return_outC:
        output['outC'] = resample_bilinear(out2, shapes.get('outA'))

    return output
